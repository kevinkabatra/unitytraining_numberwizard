﻿using UnityEngine;
using System.Collections;

public class NumberWizard : MonoBehaviour {
	int maxNumber   = 1000;
	int minNumber   = 1   ;
	int waitTime    = 3   ;
	int inputSwitch = 0   ;
	int guess       = 0   ;
	
	// Use this for initialization
	void Start () {
		StartCoroutine(Display_WelcomeMessage(maxNumber, minNumber));
	}
	
	// Update is called once per frame
	void Update () {
		if(inputSwitch == 1)
		{
			if(Input.GetKeyDown(KeyCode.UpArrow))
			{
				inputSwitch = 0;
				print ("Hmmm, thinking...");
				minNumber = maxNumber - ((maxNumber - minNumber) / 2);
				StartCoroutine(Display_Guess(maxNumber, minNumber));
			}
			else if(Input.GetKeyDown(KeyCode.DownArrow))
			{
				inputSwitch = 0;
				print ("Hmmm, thinking...");
				maxNumber = maxNumber - ((maxNumber - minNumber) / 2);
				StartCoroutine(Display_Guess(maxNumber, minNumber));
			}
			else if(Input.GetKeyDown(KeyCode.Return) 
					|| Input.GetKeyDown(KeyCode.KeypadEnter))
			{
				inputSwitch = 0;
				print ("Excellent. Goodbye.");
				Application.Quit();
			}
			else if(Input.anyKeyDown && (!Input.GetKeyDown(KeyCode.UpArrow) 
					|| !Input.GetKeyDown(KeyCode.DownArrow)
                 	|| !Input.GetKeyDown(KeyCode.Return)
                 	|| !Input.GetKeyDown(KeyCode.KeypadEnter)))
			{
				print ("You may only choose Up, Down, or Enter. Please re-enter your input");
			}
		}
		else if(inputSwitch == 2)
		{
			if(Input.GetKeyDown(KeyCode.Y))
			{
				string[] options = {
					"If I guess correctly hit enter / return...",
					"If I guess incorrectly press up arrow if your number is higher",
					"And press down arrow if your number is lower..."
				};
				
				print ("Perfect. I am now going to attempt to guess the number.");
				Display_UserInput(options);
				StartCoroutine(Display_Guess(maxNumber, minNumber));
			}
			else if(Input.GetKeyDown(KeyCode.N))
			{
				print ("OK. I will ask you again shortly.");
				StartCoroutine (Display_NumberChosenMessage(true));
			}
			else if(Input.anyKeyDown && (!Input.GetKeyDown(KeyCode.Y) 
					|| !Input.GetKeyDown(KeyCode.N)))
			{
				print ("You may only choose [y]es or [n]o. Please re-enter your input");
			}
		}
	}
	
	IEnumerator Display_WelcomeMessage (int max, int min)
	{
		print ("Welcome to Number Wizard!");
		yield return new WaitForSeconds(2) ;
		
		print ("Pick a number in your head, but do not tell me...")                ;
		print (string.Format("The largest number that you can pick is {0}." , max));
		print (string.Format("The smallest number that you can pick is {0}.", min));

		StartCoroutine (Display_NumberChosenMessage(true));
	}
	
	IEnumerator Display_NumberChosenMessage (bool wait)
	{
		if(wait)
			yield return new WaitForSeconds(waitTime);
		
		string[] options = {
			"Press y for yes.",
			"Press n for no."
		};
		
		inputSwitch = 2;
		print ("Have you chosen your number yet?");
		Display_UserInput(options);	
	}
	
	IEnumerator Display_Guess (int max, int min)
	{
		guess++;
		int num;
		
		if(min == 1 && (max - min) == 1)
			num = 1;
		else
			num = max - ((max - min) / 2);

		yield return new WaitForSeconds(waitTime);
		
		inputSwitch = 1;
		print (string.Format(
			"Guess #{0}: Is your number {1}?",
			guess, 
			num
		));
	}
	
	void Display_UserInput (string[] options)
	{
		foreach (string option in options)
		{
			print (string.Format("{0}", option));
		}
	}
}